#pragma once
#include "Player.h"
#include "Solver.h"
#include <map>
using namespace std;

//決勝用AIクラス
struct MechaAIPlayer : Player{

  MechaAIPlayer(){
    //このフラグがtrueだと、以後すべてのブロックは邪魔ブロックだと思って探索する
    attacked = false;
  }

  virtual pair<int,int> next(){
    //邪魔ブロック含まれているか調べる
    bool contain_wall = false;
    for(int y=0; y<T; ++y){
      for(int x=0; x<T; ++x){
        if(f_simulator.sim1.packs[f_simulator.sim1.turn][y][x] > S){
          contain_wall = true;
          break;
        }
      }
    }

    //今回中途半端に邪魔ブロックが入ってきた あるいは
    //前回邪魔が来ると思って探索してて、今回から邪魔ブロックが降ってこなさそう
    if((contain_wall || attacked) && f_simulator.sim2.I == 0){
      ans_queue = queue<pair<int,int> >();
      attacked = false;
    }

    //前回邪魔が来ないと思って探索してて、今回から邪魔ブロックが降ってくる
    else if(!attacked && f_simulator.sim2.I > 0){
      ans_queue = queue<pair<int,int> >();
      attacked = true;
    }

    //先読みが無い場合は次の数手を先読みしてQueueに突っ込んどく
    if(ans_queue.empty()){
      fixed_turn = f_simulator.sim1.turn;
      int left_turn = (N-1) - fixed_turn;

      vector<pair<int, int> > ans;

      //自分の盤面のブロック数
      int m_block_count = f_simulator.sim1.blockcount();
      int m_wall_count = f_simulator.sim1.wallcount();

      //相手の盤面のブロック数
      int o_block_count = f_simulator.sim2.blockcount();
      int o_wall_count = f_simulator.sim2.wallcount();

      //今どれくらい愛がある盤面なのか調べる
      int m_base_i = f_simulator.sim1.I;
      int m_max_i = 0;
      for(int n=1; n<=S/2; ++n){
        for(int x=0; x<W; ++x){
          if(f_simulator.sim1.checkstep(x,n)){
            m_max_i = max(m_max_i, f_simulator.sim1.I - m_base_i);
            f_simulator.sim1.restep();
          }
        }
      }

      //相手の盤面の愛も調べる
      int o_base_i = f_simulator.sim2.I;
      int o_max_i = 0;
      for(int n=1; n<=S/2; ++n){
        for(int x=0; x<W; ++x){
          if(f_simulator.sim2.checkstep(x,n)){
            o_max_i = max(o_max_i, f_simulator.sim2.I - o_base_i);
            f_simulator.sim2.restep();
          }
        }
      }

      //これくらい邪魔パックが降ってくると予想
      int m_expected_pack = (m_max_i + m_base_i)/20 + 1;
      int o_expected_pack = (o_max_i + o_base_i)/20 + 1;

      DEBUG(m_base_i);
      DEBUG(o_base_i);
      DEBUG(m_max_i);
      DEBUG(o_max_i);
      DEBUG(m_expected_pack);
      DEBUG(o_expected_pack);

      bool need_charge = f_simulator.sim1.Fc < setting.CHARGE_END_FIRE_COUNT;

      bool fire_flag = false;

      //1000個くらい相手に送れる時は攻撃
      if(m_max_i >= 1000){
	DEBUG(m_max_i);
	fire_flag = true;
      }

      //自分のフィールドブロックがいっぱいある時は攻撃
      if(m_block_count >= 500){
	DEBUG(m_block_count);
	fire_flag = true;
      }

      //自分のフィールドに邪魔ブロックがいっぱいある時は攻撃
      if(m_wall_count >= 300){
	DEBUG(m_wall_count);
	fire_flag = true;
      }

      //自分が相手に結構送れそうで、相手のパック数が少ない時には攻撃
      if((m_max_i - o_max_i - o_base_i) >= 300 && (o_block_count - o_wall_count) <= 30){
	fire_flag = true;
      }

      //相手が速攻を仕掛けて来そうだったらこっちもチャージして攻撃に備える
      bool enemy_will_swift_attack = false;
      if(f_simulator.current_turn < 100 && o_max_i > 300 && f_simulator.sim2.Fc <= 5){
        enemy_will_swift_attack = true;
      }

      if(fire_flag){
        LOG("FIRE!!");
        ans = fire(f_simulator.sim1, min(setting.DEPTH_FIRE_SEARCH, left_turn));
      }else if(need_charge && !enemy_will_swift_attack){
        LOG("CHARGE!!");
        ans = charge(f_simulator.sim1, min(setting.DEPTH_CHARGE_SEARCH, left_turn));
      }else{
        LOG("CHAIN!!");
        ans = chain(f_simulator.sim1, min(setting.DEPTH_CHAIN_SEARCH, left_turn));
      }

      //探索結果をQueueに入れる
      for(int i=0; i<ans.size(); ++i){
        ans_queue.push(ans[i]);
      }

      //1手だけ取り出してreturn
      if(ans_queue.empty()){
        ans = safe(f_simulator.sim1, 1);
        if(ans.empty()){
          ans.push_back(make_pair(0,0));
        }
        ans_queue.push(ans.front());
      }
      pair<int, int> ret = ans_queue.front();
      ans_queue.pop();
      f_simulator.sim1.step(ret.first, ret.second);
      return ret;
    }
    else {
      //1手だけ取り出してreturn
      pair<int, int> ret = ans_queue.front();
      ans_queue.pop();
      f_simulator.sim1.step(ret.first, ret.second);
      return ret;
    }

    //ここにきたらダメ
    ERROR("BUG BUG BUG");
    return make_pair(0,0);
  }
};
