#pragma once
#include "Player.h"
#include "Solver.h"
#include <map>
using namespace std;

//思考切替をメソッドで行う汎用プレイヤー
//GUI見ながら切替て遊ぶ事を想定
struct HumanPlayer : Player{
  bool attacked;
  bool charge_mode;
  bool chain_mode;
  bool bomb_mode;
  bool fire_mode;

  HumanPlayer(){
    attacked = false;
    charge_mode = true;
    chain_mode = false;
    bomb_mode = false;
    fire_mode = false;
  }

  void set_charge(){
    charge_mode = true;
    chain_mode = false;
    bomb_mode = false;
    fire_mode = false;
  }

  void set_bomb(){
    charge_mode = false;
    chain_mode = false;
    bomb_mode = true;
    fire_mode = false;
  }

  void set_chain(){
    charge_mode = false;
    chain_mode = true;
    bomb_mode = false;
    fire_mode = false;
  }

  void set_fire(){
    charge_mode = false;
    chain_mode = false;
    bomb_mode = false;
    fire_mode = true;
  }

  virtual pair<int,int> next(){
    Simulator& simulator = f_simulator.sim1;

    //次のブロックに邪魔ブロックが1つでも入っていれば、攻撃されたとみなす
    attacked = false;
    for(int y=0; y<T; ++y){
      for(int x=0; x<T; ++x){
        if(simulator.packs[simulator.turn][y][x] > S){
          attacked = true;
        }
      }
    }

    //攻撃されていた場合、それまでの先読みを破棄する
    if(attacked){
      ans_queue = queue<pair<int,int> >();
    }

    //先読みが無い場合は次の数手を先読みしてQueueに突っ込んどく
    if(ans_queue.empty()){

      fixed_turn = simulator.turn;
      int left_turn = (N-1) - fixed_turn;
      vector<pair<int, int> > ans;

      if(charge_mode){
        ans = charge(simulator, min(setting.DEPTH_CHARGE_SEARCH, left_turn));
      }
      else if(bomb_mode){
        ans = bomb(simulator, min(setting.DEPTH_BOMB_SEARCH, left_turn));
      }
      else if(chain_mode){
        ans = chain(simulator, min(setting.DEPTH_CHAIN_SEARCH, left_turn));
      }
      else if(fire_mode){
        ans = fire(simulator, min(setting.DEPTH_FIRE_SEARCH, left_turn));
      }

      for(int i=0; i<ans.size(); ++i){
        ans_queue.push(ans[i]);
      }

      //1手だけ取り出してreturn
      if(ans_queue.empty()){
	return make_pair(0,0);
      }
      pair<int, int> ret = ans_queue.front();
      ans_queue.pop();
      simulator.step(ret.first, ret.second);
      return ret;
    }
    else {
      //1手だけ取り出してreturn
      pair<int, int> ret = ans_queue.front();
      ans_queue.pop();
      simulator.step(ret.first, ret.second);
      return ret;
    }

    //ここにきたらダメェ
    ERROR("BUG BUG BUG");
    return make_pair(0,0);
  }
};