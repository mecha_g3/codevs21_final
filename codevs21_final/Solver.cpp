#include "Solver.h"
#include "Logger.h"
#include <set>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <ctime>
#include <queue>
#include <iterator>
#define TURN_CHECK if(fixed_turn != simulator.turn){ ERROR("Turn Error."); }
using namespace std;

Solver::Solver(vector<vector<vector<int> > > packs){
  attacked = false;
}

Solver::Solver(){
  attacked = false;
}

void Solver::setpacks(vector<vector<vector<int> > > packs){
  f_simulator.init(packs);
}

// ================================================================================
// bombAI 不定形で爆弾を作る
// ================================================================================
long long Solver::calc_bomb_score(Simulator& simulator){
  long long ret = 0;

  if(setting.RATIO_BOMB_BLOCK_COUNT){
    ret += simulator.blockcount() * setting.RATIO_BOMB_BLOCK_COUNT;
  }

  int max_erase_count       = 0;
  int max_last_erase_count  = 0;

  long long max_raw_score   = 0;

  for(int n=1; n<=S/2; ++n){
    if(simulator.checkstep(W/2-1,n)){
      max_erase_count       = max(max_erase_count,      simulator.max_erase_count);
      max_last_erase_count  = max(max_last_erase_count, simulator.last_erase_count);
      max_raw_score         = max(max_raw_score,        simulator.raw_score);
      simulator.restep();
    }
    ret += max_erase_count      * setting.RATIO_BOMB_CHECKSTEP_MAX_ERASE_COUNT;
    ret += max_last_erase_count * setting.RATIO_BOMB_CHECKSTEP_MAX_LAST_ERASE_COUNT;
    ret += max_raw_score        * setting.RATIO_BOMB_CHECKSTEP_MAX_RAW_SCORE;
  }

  return ret;
}

//スコアによって上位n個をフィルタ
vector<vector<pair<int,int> > > Solver::bomb_score_filter(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int n){
  if(nodes.size() <= n){
    return nodes;
  }

  set<unsigned long> exist;
  TURN_CHECK;
  vector<pair<long long,int> > score_nodes(nodes.size());
  for(int i=0; i<nodes.size(); ++i){
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
    }

    score_nodes[i].second = i;

    unsigned int hash = simulator.hash();
    if(exist.find(hash) == exist.end()){
      score_nodes[i].first = calc_bomb_score(simulator);
      exist.insert(hash);
    }
    else{
      score_nodes[i].first  = 0;
    }

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }
  }
  TURN_CHECK;

  sort(score_nodes.rbegin(), score_nodes.rend());

  DEBUG(score_nodes[0].first);

  int M = min((int)score_nodes.size(), n);
  vector<vector<pair<int,int> > > ret(M);
  for(int i=0; i<M; ++i){
    ret[i] = nodes[ score_nodes[i].second ];
  }

  return ret;
}

vector<vector<pair<int,int> > > Solver::rec_bomb(Simulator& simulator, const vector<vector<pair<int,int> > >& before_nodes, int depth){
  DEBUG(depth);
  DEBUG(before_nodes.size());

  if(depth <= 0){
    return before_nodes;
  }

  vector<vector<pair<int,int> > > nodes = bomb_score_filter(simulator, before_nodes, setting.WIDTH_BOMB_SEARCH);
  vector<vector<pair<int,int> > > next_nodes;
  next_nodes.reserve(setting.WIDTH_BOMB_SEARCH * (W + T) * 4);

  bool found = false;
  int max_e = 0;

  TURN_CHECK;
  for(int i=0; i<nodes.size(); ++i){
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
    }

    for(int n=1; n<=S/2; ++n){
      if(simulator.checkstep(W/2-1,n)){
        max_e = max(max_e, simulator.max_erase_count);
        if(simulator.max_erase_count >= setting.BOMB_END_ERASE_COUNT){
          found = true;
        }
        simulator.restep();
      }
    }

    for(int x=W/2; x<W; ++x){
      for(int r=0; r<4; ++r){
        if(simulator.step(x,r, attacked)){
          simulator.restep();
          vector<pair<int,int> > node(nodes[i]);
          node.push_back(make_pair(x,r));
          next_nodes.push_back(node);
        }
      }
    }

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }
  }
  TURN_CHECK;

  DEBUG(max_e);
  if(found) return nodes;
  return rec_bomb(simulator, next_nodes, depth-1);
}

vector<pair<int,int> > Solver::bomb(Simulator& simulator, int max_depth){
  //1手先を突っ込む
  vector<vector<pair<int,int> > > nodes;
  for(int x=W/2; x<W; ++x)for(int r=0; r<4; ++r){
    if(simulator.step(x,r,attacked)){
      vector<pair<int,int> > node;
      node.push_back(make_pair(x,r));
      nodes.push_back(node);
      simulator.restep();
    }
  }
  nodes = rec_bomb(simulator, nodes, max_depth-1);
  nodes = bomb_score_filter(simulator, nodes, 1);
  return nodes.front();
}





// ================================================================================
// 連鎖AI できるだけ相手に邪魔ブロックが降るように探索
// ================================================================================
vector<pair<int,int> > Solver::chain(Simulator& simulator, int max_depth){

  vector<vector<pair<int,int> > > nodes;

  for(int x=-T; x<W; ++x){
    for(int r=0; r<4; ++r){
      if(simulator.step(x,r,attacked)){
        vector<pair<int,int> > node;
        node.push_back(make_pair(x,r));
        nodes.push_back(node);
        simulator.restep();
      }
    }
  }

  nodes = rec_chain(simulator, nodes, max_depth-1);
  nodes = chain_score_filter(simulator, nodes, 1);

  if(nodes.empty()){
    return safe(simulator, 1);
  }

  return nodes.front();
}

vector<vector<pair<int,int> > > Solver::chain_score_filter(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int n){

  if(nodes.size() < n){
    return nodes;
  }

  set<unsigned int> exist;
  vector<pair<int,int> > score_nodes(nodes.size());

  for(int i=0; i<nodes.size(); ++i){
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
    }

    score_nodes[i].second = i;
    score_nodes[i].first  = 0;

    int base_score = simulator.blockcount();
    int before_i = simulator.I;
    unsigned int hash = simulator.hash();

    if(exist.find(hash) == exist.end()){
      exist.insert(hash);
      for(int n=1; n<=S/2; ++n){
        for(int x=0; x<3; ++x){
          if(simulator.checkstep(x,n)){
	    const int score = base_score + (simulator.I - before_i) * 1000;
            score_nodes[i].first = max(score_nodes[i].first, score);
            simulator.restep();
          }
        }
      }
    }

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }
  }

  sort(score_nodes.rbegin(), score_nodes.rend());
  int M = min((int)score_nodes.size(), n);
  vector<vector<pair<int,int> > > ret(M);
  for(int i=0; i<M; ++i){
    ret[i] = nodes[ score_nodes[i].second ];
  }

  return ret;
}

vector<vector<pair<int,int> > > Solver::rec_chain(Simulator& simulator, const vector<vector<pair<int,int> > >& before_nodes, int depth){
  DEBUG(depth);
  if(depth <= 0){
    return before_nodes;
  }

  vector<vector<pair<int,int> > > nodes = chain_score_filter(simulator, before_nodes, setting.WIDTH_CHAIN_SEARCH);
  vector<vector<pair<int,int> > > next_nodes;
  next_nodes.reserve(setting.WIDTH_CHAIN_SEARCH * 4 * W);

  for(int i=0; i<nodes.size(); ++i){
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
    }

    for(int x=-T; x<W; ++x){
      for(int r=0; r<4; ++r){
        if(simulator.step(x,r,attacked)){
          vector<pair<int,int> > node(nodes[i]);
          node.push_back(make_pair(x,r));
          next_nodes.push_back(node);
          simulator.restep();
        }
      }
    }

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }
  }

  return rec_chain(simulator, next_nodes, depth-1);
}



// ================================================================================
// ファイヤーAI 起爆します
// ================================================================================
vector<pair<int,int> > Solver::fire(Simulator& simulator, int max_depth){

  vector<vector<pair<int,int> > > nodes;

  for(int x=-T; x<W; ++x)for(int r=0; r<4; ++r){
    if(simulator.step(x,r,attacked)){
      vector<pair<int,int> > node;
      node.push_back(make_pair(x,r));
      nodes.push_back(node);
      simulator.restep();
    }
  }

  nodes = rec_fire(simulator, nodes, max_depth-1);
  nodes = fire_score_filter(simulator, nodes, setting.WIDTH_FIRE_SEARCH);

  vector<pair<int,int> > ans;
  int ans_score = 0;

  for(int i=0; i<nodes.size(); ++i){
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
      for(int x=-T; x<W; ++x)for(int r=0; r<4; ++r)if(simulator.step(x,r,attacked)){
        int score = simulator.I;
        if(ans_score <= score){
          DEBUG(score);
          ans_score = score;
          ans.clear();
          for(int k=0; k<=j; ++k){
            ans.push_back(nodes[i][k]);
          }
          ans.push_back(make_pair(x,r));
        }
        simulator.restep();
      }
    }
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }
  }

  if(ans.empty()){
    return safe(simulator, 1);
  }
  return ans;
}



vector<vector<pair<int,int> > > Solver::fire_score_filter(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int n){
  LOG("Filtering..");
  if(nodes.size() < n){
    return nodes;
  }

  set<unsigned int> exist;
  vector<pair<int ,int> > score_nodes(nodes.size());

  for(int i=0; i<nodes.size(); ++i){
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
    }

    score_nodes[i].second = i;
    score_nodes[i].first  = 0;

    unsigned int hash = simulator.hash();

    if(exist.find(hash) == exist.end()){
      exist.insert(hash);
      for(int n=1; n<=S/2; ++n){
        for(int x=0; x<W; ++x)if(simulator.checkstep(x,n)){
          score_nodes[i].first = max(score_nodes[i].first, simulator.I);
          simulator.restep();
        }
      }
    }

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }
  }

  sort(score_nodes.rbegin(), score_nodes.rend());

  DEBUG(score_nodes[0].first);

  int N = min((int)score_nodes.size(), n);
  vector<vector<pair<int,int> > > ret(N);
  for(int i=0; i<N; ++i){
    ret[i] = nodes[ score_nodes[i].second ];
  }

  return ret;
}

vector<vector<pair<int,int> > > Solver::rec_fire(Simulator& simulator, const vector<vector<pair<int,int> > >& before_nodes, int depth){
  DEBUG(depth);
  DEBUG(before_nodes.size());

  if(depth <= 0){
    return before_nodes;
  }

  vector<vector<pair<int,int> > > nodes = fire_score_filter(simulator, before_nodes, setting.WIDTH_FIRE_SEARCH);
  vector<vector<pair<int,int> > > next_nodes;
  next_nodes.reserve(setting.WIDTH_FIRE_SEARCH * W * 4);

  for(int i=0; i<nodes.size(); ++i){
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
    }

    for(int x=1-T; x<W; ++x){
      for(int r=0; r<4; ++r){
        if(simulator.step(x,r,attacked)){
          vector<pair<int,int> > node(nodes[i]);
          node.push_back(make_pair(x,r));
          next_nodes.push_back(node);
          simulator.restep();
        }
      }
    }

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }
  }

  return rec_fire(simulator, next_nodes, depth-1);
}


// ================================================================================
// セーフAI とにかくブロックが減るように降らす
// ================================================================================
long long Solver::calc_safe_score(Simulator& simulator){
  long long ret = 1000000000000LL;
  ret -= simulator.blockcount();
  for(int x=1-T; x<W; ++x){
    for(int r=0; r<4; ++r){
      if(simulator.step(x,r,attacked)){
	ret -= simulator.blockcount() * 1000;
        simulator.restep();
      }
    }
  }
  return ret;
}

vector<vector<pair<int,int> > > Solver::safe_score_filter(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int n){
  if(nodes.size() < n){
    return nodes;
  }

  TURN_CHECK;
  vector<pair<long long,int> > score_nodes(nodes.size());
  for(int i=0; i<nodes.size(); ++i){
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
    }

    score_nodes[i].first  = calc_safe_score(simulator);
    score_nodes[i].second = i;

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }
  }
  TURN_CHECK;

  sort(score_nodes.rbegin(), score_nodes.rend());

  DEBUG(score_nodes[0].first);

  int M = min((int)score_nodes.size(), setting.WIDTH_SAFE_SEARCH);
  vector<vector<pair<int,int> > > ret(M);
  for(int i=0; i<M; ++i){
    ret[i] = nodes[ score_nodes[i].second ];
  }

  return ret;
}

vector<vector<pair<int,int> > > Solver::rec_safe(Simulator& simulator, const vector<vector<pair<int,int> > >& before_nodes, int depth){
  DEBUG(depth);
  DEBUG(before_nodes.size());
  if(depth <= 0){
    return before_nodes;
  }

  vector<vector<pair<int,int> > > nodes = safe_score_filter(simulator, before_nodes, setting.WIDTH_SAFE_SEARCH);
  vector<vector<pair<int,int> > > next_nodes;
  next_nodes.reserve(setting.WIDTH_SAFE_SEARCH * W * 4);
  TURN_CHECK;
  for(int i=0; i<nodes.size(); ++i){
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
    }

    for(int x=1-T; x<W; ++x){
      for(int r=0; r<4; ++r){
        if(simulator.step(x,r,attacked)){
          vector<pair<int,int> > node(nodes[i]);
          node.push_back(make_pair(x,r));
          next_nodes.push_back(node);
          simulator.restep();
        }
      }
    }

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }
  }
  TURN_CHECK;
  return rec_safe(simulator, next_nodes, depth-1);
}

vector<pair<int,int> > Solver::safe(Simulator& simulator, int max_depth){
  vector<vector<pair<int,int> > > nodes;
  for(int x=1-T; x<W; ++x){
    for(int r=0; r<4; ++r){
      if(simulator.step(x,r,attacked)){
        vector<pair<int,int> > node;
        node.push_back(make_pair(x,r));
        nodes.push_back(node);
        simulator.restep();
      }
    }
  }

  nodes = rec_safe(simulator, nodes, max_depth-1);
  nodes = safe_score_filter(simulator, nodes, 1);

  if(nodes.empty()){
    vector<pair<int,int> > ret;
    return ret;
  }
  else{
    return nodes.front();
  }
}












// ================================================================================
// チャージAI Fcの値を出来るだけ短いターンで増やす
// 決勝では使わないだろう
// ================================================================================
long long Solver::calc_charge_score(Simulator& simulator){
  long long ret = 0;
  if(setting.RATIO_CHARGE_BLOCK_COUNT){
    ret += simulator.blockcount() * setting.RATIO_CHARGE_BLOCK_COUNT;
  }
  const int before_fire_count = simulator.Fc;
  for(int x=1-T; x<W; ++x){
    for(int r=0; r<4; ++r){
      if(simulator.step(x,r,attacked)){
        ret += (simulator.Fc - before_fire_count) * setting.RATIO_CHARGE_STEP_DELTA_FIRE_COUNT;
        ret += simulator.raw_score;
        simulator.restep();
      }
    }
  }
  return ret;
}

vector<vector<pair<int,int> > > Solver::charge_score_filter(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int n){
  if(nodes.size() < n){
    return nodes;
  }

  TURN_CHECK;
  set<unsigned int> exist;
  int cut_count = 0;

  vector<pair<long long,int> > score_nodes(nodes.size());
  for(int i=0; i<nodes.size(); ++i){
    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
    }

    score_nodes[i].second = i;
    unsigned int hash = simulator.hash();
    if(exist.find(hash) == exist.end()){
      exist.insert(hash);
      score_nodes[i].first  = calc_charge_score(simulator);
    }
    else{
      score_nodes[i].first  = 0;
      cut_count++;
    }

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }
  }
  TURN_CHECK;

  sort(score_nodes.rbegin(), score_nodes.rend());

  DEBUG(score_nodes[0].first);
  DEBUG(cut_count);

  int N = min((int)score_nodes.size(), setting.WIDTH_CHARGE_SEARCH);
  vector<vector<pair<int,int> > > ret(N);
  for(int i=0; i<N; ++i){
    ret[i] = nodes[ score_nodes[i].second ];
  }

  return ret;
}

vector<vector<pair<int,int> > > Solver::rec_charge(Simulator& simulator, const vector<vector<pair<int,int> > >& before_nodes, int depth){
  DEBUG(depth);
  DEBUG(before_nodes.size());
  if(depth <= 0){
    return before_nodes;
  }

  vector<vector<pair<int,int> > > nodes = charge_score_filter(simulator, before_nodes, setting.WIDTH_CHARGE_SEARCH);
  vector<vector<pair<int,int> > > next_nodes;
  next_nodes.reserve(setting.WIDTH_CHARGE_SEARCH * W * 4);

  const int before_fc = simulator.Fc;
  bool found = false;
  vector<vector<pair<int,int> > > found_nodes;

  TURN_CHECK;
  for(int i=0; i<nodes.size(); ++i){

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.step(nodes[i][j].first, nodes[i][j].second, attacked);
    }

    for(int x=1-T; x<W; ++x){
      for(int r=0; r<4; ++r){
        if(simulator.step(x,r,attacked)){
          const int after_fc = simulator.Fc;
          vector<pair<int,int> > node(nodes[i]);
          node.push_back(make_pair(x,r));

          if(before_fc < after_fc){
            found = true;
            found_nodes.push_back(node);
          }
          else{
            next_nodes.push_back(node);
          }
          simulator.restep();
        }
      }
    }

    for(int j=0; j<nodes[i].size(); ++j){
      simulator.restep();
    }

  }
  TURN_CHECK;
  if(found) return found_nodes;
  else      return rec_charge(simulator, next_nodes, depth-1);
}

vector<pair<int,int> > Solver::charge(Simulator& simulator, int max_depth){
  vector<vector<pair<int,int> > > nodes;
  for(int x=1-T; x<W; ++x){
    for(int r=0; r<4; ++r){
      if(simulator.step(x,r,attacked)){
        vector<pair<int,int> > node;
        node.push_back(make_pair(x,r));
        nodes.push_back(node);
        simulator.restep();
      }
    }
  }

  nodes = rec_charge(simulator, nodes, max_depth-1);

  int same_score_count = 0;
  vector<pair<int,int> > ans;
  long long ans_score = 0;
  const int before_fc = simulator.Fc;

  for(int i=0; i<nodes.size(); ++i){
    const int N = (int)nodes[i].size()-1;
    for(int j=0; j<N; ++j){
      simulator.step(nodes[i][j].first,nodes[i][j].second,attacked);
      for(int x=1-T; x<W; ++x){
        for(int r=0; r<4; ++r){
          if(simulator.step(x,r,attacked)){
            long long score = 0;
            const int after_fc = simulator.Fc;
            score += simulator.blockcount() * setting.RATIO_CHARGE_FINISH_BLOCK_COUNT;
            score += (after_fc - before_fc) * setting.RATIO_CHARGE_FINISH_DELTA_FIRE_COUNT;
            if(ans_score < score){
              same_score_count = 0;
              ans_score = score;
              ans.clear();
              for(int k=0; k<=j; ++k){
                ans.push_back(nodes[i][k]);
              }
              ans.push_back(make_pair(x,r));
            }
            else if(ans_score == score){
              same_score_count++;
            }
            simulator.restep();
          }
        }
      }
    }
    for(int j=0; j<N; ++j){
      simulator.restep();
    }
  }
  DEBUG(same_score_count);

  if(ans.empty()){
    return safe(simulator, 1);
  }
  return ans;
}
