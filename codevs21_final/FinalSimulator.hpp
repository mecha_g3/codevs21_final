#pragma once
#include "Simulator.hpp"
#include "Logger.h"
#include <vector>
using namespace std;

struct FinalSimulator{
  int current_turn;
  bool game_over;

  Simulator& sim1;
  Simulator& sim2;

  FinalSimulator()
    : sim1(* new Simulator()), sim2(* new Simulator()){
    current_turn = 0;
    game_over = false;
  }

  ~FinalSimulator(){
    delete &sim1;
    delete &sim2;
  }

  void init(vector<vector<vector<int> > > packs){
    sim1.setpacks(packs);
    sim2.setpacks(packs);
  }

  void update(){
    int dI = abs(sim1.I - sim2.I);
    if(sim1.I > sim2.I){
      for(int y=0; y<sim1.T; ++y){
        for(int x=0; x<sim1.T; ++x){
          if(dI > 0 && sim2.packs[sim2.turn][y][x] == 0){
            sim2.packs[sim2.turn][y][x] = sim2.S+1;
            dI--;
          }
        }
      }

      sim1.I = dI;
      sim2.I = 0;
    }
    else if(sim1.I < sim2.I){
      for(int y=0; y<sim1.T; ++y){
        for(int x=0; x<sim1.T; ++x){
          if(dI > 0 && sim1.packs[sim1.turn][y][x] == 0){
            sim1.packs[sim1.turn][y][x] = sim1.S+1;
            dI--;
          }
        }
      }

      sim1.I = 0;
      sim2.I = dI;
    }
    else{
      sim1.I = 0;
      sim2.I = 0;
    }

    current_turn++;
    DEBUG(current_turn);
    DEBUG(sim1.turn);
    DEBUG(sim2.turn);
  }
};
