#include "../codevs21_final/Simulator.hpp"
#include "../codevs21_final/Logger.h"
#include "../codevs21_final/Solver.h"
#include "../codevs21_final/HumanPlayer.h"
#include "../codevs21_final/MechaAIPlayer.h"
#include "../codevs21_final/FinalSimulator.hpp"
#include "InputHandler.h"
#include "DxLib.h"


int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow ){
  Logger::Initialize();

  ChangeWindowMode(TRUE);
  SetGraphMode(1200, 640, 32);
	if(DxLib_Init() == -1){
		return -1;
	}
  AllocConsole();
  freopen("CONOUT$", "w", stdout); 
  freopen("CONIN$", "r", stdin);

  SetDrawScreen(DX_SCREEN_BACK);


	string root = "../resource/";
	string input_path = root + "input/1372343675.txt";

  ifstream ifs(input_path);
  int wid,hei,size,sum,step;
  ifs >> wid >> hei >> size >> sum >> step;

  vector<vector<vector<int> > > packs(step, vector<vector<int> > (size, vector<int>(size, 0)));
  for(int turn=0; turn<step; ++turn){
    for(int y=0; y<size; ++y){
      for(int x=0; x<size; ++x){
        int num;
        ifs >> num;
        if(num > sum)num = 0; // for final rule
        //if(!num)num = sum+1;
        packs[turn][y][x] = num;
      }
    }
    string END;
    ifs >> END;
  }

  string img_root = "../resource/img/";
  int bg_handle = LoadGraph((img_root + "background.png").c_str());
  int block_handle[50] = {0};
  block_handle[1] = LoadGraph((img_root + "1.png").c_str());
  block_handle[2] = LoadGraph((img_root + "2.png").c_str());
  block_handle[3] = LoadGraph((img_root + "3.png").c_str());
  block_handle[4] = LoadGraph((img_root + "4.png").c_str());
  block_handle[5] = LoadGraph((img_root + "5.png").c_str());
  block_handle[6] = LoadGraph((img_root + "6.png").c_str());
  block_handle[7] = LoadGraph((img_root + "7.png").c_str());
  block_handle[8] = LoadGraph((img_root + "8.png").c_str());
  block_handle[9] = LoadGraph((img_root + "9.png").c_str());
  block_handle[10] = LoadGraph((img_root + "10.png").c_str());
  block_handle[11] = LoadGraph((img_root + "11.png").c_str());
  block_handle[12] = LoadGraph((img_root + "12.png").c_str());
  block_handle[13] = LoadGraph((img_root + "13.png").c_str());
  block_handle[14] = LoadGraph((img_root + "14.png").c_str());
  block_handle[15] = LoadGraph((img_root + "15.png").c_str());
  block_handle[31] = LoadGraph((img_root + "31.png").c_str());

  FinalSimulator simulator;
  simulator.init(packs);
  HumanPlayer human;
  MechaAIPlayer mecha_ai;


  human.setpacks(packs);
  mecha_ai.setpacks(packs);
  
  while(!ProcessMessage() && !CheckHitKey(KEY_INPUT_ESCAPE)){
    WaitKey();
    if(CheckHitKey(KEY_INPUT_Z)){
      human.set_charge();
    }else if(CheckHitKey(KEY_INPUT_X)){
      human.set_bomb();
    }else if(CheckHitKey(KEY_INPUT_C)){
      human.set_chain();
    }else if(CheckHitKey(KEY_INPUT_V)){
      human.set_fire();
    }

    human.f_simulator.sim1.setpack(human.f_simulator.sim2.turn, simulator.sim1.packs[simulator.current_turn]);
    human.f_simulator.sim2.setpack(human.f_simulator.sim1.turn, simulator.sim2.packs[simulator.current_turn]);

    mecha_ai.f_simulator.sim1.setpack(mecha_ai.f_simulator.sim1.turn, simulator.sim2.packs[simulator.current_turn]);
    mecha_ai.f_simulator.sim2.setpack(mecha_ai.f_simulator.sim2.turn, simulator.sim1.packs[simulator.current_turn]);

    pair<int,int> ans1 = human.next();
    pair<int,int> ans2 = mecha_ai.next();

    simulator.sim1.step(ans1.first,ans1.second);
    simulator.sim2.step(ans2.first,ans2.second);

    simulator.update();

    // �\��
    DrawGraph(0, 0, bg_handle, FALSE);
    // 1p
    for(int y=0; y<simulator.sim1.H; ++y){
      for(int x=0; x<simulator.sim1.W; ++x){
        const int num = simulator.sim1.field[y][x];
        if(num){
          const int draw_y = 30 + (simulator.sim1.H - 1 - y) * 14;
          const int draw_x = 99 + x * 14;
          DrawGraph(draw_x, draw_y, block_handle[num], FALSE);
        }
      }
    }

    // 2p
    for(int y=0; y<simulator.sim2.H; ++y){
      for(int x=0; x<simulator.sim2.W; ++x){
        const int num = simulator.sim2.field[y][x];
        if(num){
          const int draw_y = 30 + (simulator.sim2.H - 1 - y) * 14;
          const int draw_x = 687 + x * 14;
          DrawGraph(draw_x, draw_y, block_handle[num], FALSE);
        }
      }
    }

    // Info
    DrawFormatString(400, 80, GetColor(32,32,32), "Turn  : %d", simulator.sim1.turn);
    DrawFormatString(400, 100, GetColor(32,32,32), "Score : %g", double(simulator.sim1.score));
    DrawFormatString(400, 120, GetColor(32,32,32), "Fc    : %d", simulator.sim1.Fc);
    DrawFormatString(400, 140, GetColor(32,32,32), "I     : %d", simulator.sim1.I);
    DrawFormatString(400, 160, GetColor(32,32,32), "Chain : %d", simulator.sim1.chain);
    DrawFormatString(400, 180, GetColor(32,32,32), "Block : %d", simulator.sim1.blockcount());
    DrawFormatString(400, 200, GetColor(32,32,32), "Erase : %d", simulator.sim1.erase_count);

    DrawFormatString(990, 80, GetColor(32,32,32), "Turn  : %d", simulator.sim2.turn);
    DrawFormatString(990, 100, GetColor(32,32,32), "Score : %g", double(simulator.sim2.score));
    DrawFormatString(990, 120, GetColor(32,32,32), "Fc    : %d", simulator.sim2.Fc);
    DrawFormatString(990, 140, GetColor(32,32,32), "I     : %d", simulator.sim2.I);
    DrawFormatString(990, 160, GetColor(32,32,32), "Chain : %d", simulator.sim2.chain);
    DrawFormatString(990, 180, GetColor(32,32,32), "Block : %d", simulator.sim2.blockcount());
    DrawFormatString(990, 200, GetColor(32,32,32), "Erase : %d", simulator.sim2.erase_count);

    ScreenFlip();
  }

	DxLib_End();
	return 0;
}
