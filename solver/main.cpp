#include<iostream>
#include"../codevs21_final/Simulator.hpp"
#include"../codevs21_final/Simulator.hpp"
#include"../codevs21_final/MechaAIPlayer.h"

using namespace std;

int main(){
  Logger::Initialize();

  int wid,hei,size,sum,step;
  cin >> wid >> hei >> size >> sum >> step;

  vector<vector<vector<int> > > packs(step, vector<vector<int> > (size, vector<int>(size, 0)));
  for(int turn=0; turn<step; ++turn){
    for(int y=0; y<size; ++y){
      for(int x=0; x<size; ++x){
        int num;
        cin >> num;
        packs[turn][y][x] = num;
      }
    }
    string END;
    cin >> END;
  }

  MechaAIPlayer mecha_ai;
  mecha_ai.setpacks(packs);

  while(true){
    pair<int,int> ans = mecha_ai.next();
    cout << ans.first << " " << ans.second << endl;

    vector<vector<int> > m_pack(size, vector<int>(size, 0));
    for(int y=0; y<size; ++y){
      for(int x=0; x<size; ++x){
        int num;
        cin >> num;
        m_pack[y][x] = num;
      }
    }
    string END;
    cin >> END;

    vector<vector<int> > o_pack(size, vector<int>(size, 0));
    for(int y=0; y<size; ++y){
      for(int x=0; x<size; ++x){
        int num;
        cin >> num;
        o_pack[y][x] = num;
      }
    }
    cin >> END;

    int o_x, o_r;
    cin >> o_x >> o_r;
    mecha_ai.f_simulator.sim2.step(o_x, o_r);

    mecha_ai.f_simulator.sim1.setpack(mecha_ai.f_simulator.sim1.turn, m_pack);
    mecha_ai.f_simulator.sim2.setpack(mecha_ai.f_simulator.sim2.turn, o_pack);

    LOG("SELF");
    mecha_ai.f_simulator.sim1.dump();

    LOG("ENEMY");
    mecha_ai.f_simulator.sim2.dump();


    DEBUG("C");

    int m_stock = 0;
    int o_stock = 0;

    cin >> o_stock >> m_stock;
    DEBUG("D");

    mecha_ai.f_simulator.sim1.I = m_stock;
    mecha_ai.f_simulator.sim2.I = o_stock;

    DEBUG("A");
  }

  //TODO io
  return 0;
}