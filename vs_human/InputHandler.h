#pragma once
#include "DxLib.h"

class InputHandler{
  static int buf[256];

public:
  void init();
  void update();
  bool isHit(int keycode);
};
