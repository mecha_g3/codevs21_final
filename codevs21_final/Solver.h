#pragma once
#include "../codevs21_final/Simulator.hpp"
#include "../codevs21_final/FinalSimulator.hpp"
#include "SolverSetting.h"
#include <vector>
#include <map>
#include <queue>
using namespace std;

struct Solver{
  static const int W = 20;
  static const int H = 36+5;
  static const int T = 5;
  static const int S = 30;
  static const int N = 1000;
  static const int P = 35;
  static const int Th= 10000;

  SolverSetting setting;
  FinalSimulator f_simulator;

  int fixed_turn;
  bool fire_charged;
  bool bomb_created;
  bool chain_created;
  bool attacked;
  
  Solver(vector<vector<vector<int> > > packs);
  Solver();
  void setpacks(vector<vector<vector<int > > > packs);

  queue<pair<int,int> > ans_queue;

  vector<pair<int,int> > charge(Simulator& simulator, int max_depth);
  long long calc_charge_score(Simulator& simulator);
  vector<vector<pair<int,int> > > charge_score_filter(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int n);
  vector<vector<pair<int,int> > > rec_charge(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int depth);

  vector<pair<int,int> > bomb(Simulator& simulator, int max_depth);
  long long calc_bomb_score(Simulator& simulator);
  vector<vector<pair<int,int> > > bomb_score_filter(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int n);
  vector<vector<pair<int,int> > > rec_bomb(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int depth);


  vector<pair<int,int> > chain(Simulator& simulator, int max_depth);
  vector<vector<pair<int,int> > > chain_score_filter(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int n);
  vector<vector<pair<int,int> > > rec_chain(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int depth);


  vector<pair<int,int> > fire(Simulator& simulator, int max_depth);
  long long calc_fire_score(Simulator& simulator);
  vector<vector<pair<int,int> > > fire_score_filter(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int n);
  vector<vector<pair<int,int> > > rec_fire(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int depth);


  vector<pair<int,int> > safe(Simulator& simulator, int max_depth);
  long long calc_safe_score(Simulator& simulator);
  vector<vector<pair<int,int> > > safe_score_filter(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int n);
  vector<vector<pair<int,int> > > rec_safe(Simulator& simulator, const vector<vector<pair<int,int> > >& nodes, int depth);
};
