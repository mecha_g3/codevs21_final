#include<iostream>
#include<sstream>
#include<fstream>
#include"Logger.h"
#include"Simulator.hpp"

using namespace std;

int main(int argc, char *argv[]){
  Logger::Initialize();

  string root = "../resource/";
  string input_path = root + "input/" + argv[1];
  string output_path = root + "output/" + argv[1];
  string result_path = root + "result/" + argv[1];

  ifstream ifs(input_path);
  int wid,hei,size,sum,step;
  ifs >> wid >> hei >> size >> sum >> step;

  vector<vector<vector<int> > > packs(step, vector<vector<int> > (size, vector<int>(size, 0)));
  for(int turn=0; turn<step; ++turn){
    for(int y=0; y<size; ++y){
      for(int x=0; x<size; ++x){
        int num;
        ifs >> num;
        packs[turn][y][x] = num;
      }
    }
    string END;
    ifs >> END;
  }

  // do nothing
  return 0;
}