#include "InputHandler.h"

int InputHandler::buf[256];

void InputHandler::init(){
  memset(buf, 0, sizeof(buf));
}

void InputHandler::update(){
  char GetHitKeyStateAll_Key[256];
  GetHitKeyStateAll(GetHitKeyStateAll_Key);
  for(int i=0;i<256;i++){
    if(GetHitKeyStateAll_Key[i]==1) buf[i]++;
    else                            buf[i]=0;
  }
}

bool InputHandler::isHit(int keycode){
  return buf[keycode] > 0;
}
